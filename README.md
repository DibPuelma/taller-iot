## Método de instalación

1.- Clonar repositorio

2.- Navegar hasta el repositorio en la terminal

3.- Utilizar ```npm install``` para instalar todas las dependencias

4.- Utilizar ```npm start``` para iniciar el servidor

5.- Si no se abre automáticamente, abrir en el navegador http://localhost:3000/
