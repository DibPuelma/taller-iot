import React, { Component } from 'react';

export default class DataShow extends Component {
  state = {
    data: null,
    loading: true,
  }
  componentDidMount(){
    this.listener = this.props.firebase.db.ref('activities').on('value', (snapshot) => {
      this.setState({data: snapshot.val(), loading: false});
    } )
  }

  componentWillUnmount(){
    this.listener()
  }

  render(){
    const { loading, data } = this.state
    if(loading){
      return(
        <div> Loading </div>
      )
    }
    return(
      <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center', flexWrap: 'wrap'}}>
      {data.map((team) => {
        return(
          <div key={team.team} style={{margin: '1em', padding: '1em', border: '1px solid'}}>
            <div><b>Equipo {team.team + 1}</b></div>
            <div>Temperatura: {team.temp}ºC</div>
            <div>Ventilador prendido: {team.isOn ? 'Sí' : 'No'}</div>
          </div>
        )
      })}
      </div>
    );
  }
}
