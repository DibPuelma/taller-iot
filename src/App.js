import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import DataShow from './DataShow';
import { FirebaseContext } from './Firebase';

class App extends Component {
  render() {
    return (
      <FirebaseContext.Consumer>
        {firebase => <DataShow firebase={firebase}/>}
      </FirebaseContext.Consumer>
    );
  }
}

export default App;
