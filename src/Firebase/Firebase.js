import app from 'firebase/app';
import 'firebase/database';
const config = {
    apiKey: "AIzaSyB49zEJ0h404D8IBM646kOhehCCIKUDF4Y",
    authDomain: "taller-iot-33186.firebaseapp.com",
    databaseURL: "https://taller-iot-33186.firebaseio.com",
    projectId: "taller-iot-33186",
    storageBucket: "taller-iot-33186.appspot.com",
    messagingSenderId: "917821654294"
  };

class Firebase {
  constructor() {
    app.initializeApp(config);
    this.db = app.database()
  }
}

export default Firebase;
